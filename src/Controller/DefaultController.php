<?php

namespace App\Controller;

use App\Entity\Shoes;
use App\Entity\Blog;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $shoes = $em->getRepository(Shoes::class)->findBy(['popular_arrivals_check' => true]);
        $blogs = $em->getRepository(Blog::class)->findAll();

        return $this->render('base.html.twig', [
            'shoes' => $shoes,
            'blogs' => $blogs,
        ]);
    }
}
