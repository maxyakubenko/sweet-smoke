<?php

namespace App\Entity;

use App\Repository\ShoesRepository;
use Doctrine\ORM\Mapping as ORM;
use Sonata\MediaBundle\Model\MediaInterface;

/**
 * @ORM\Entity(repositoryClass=ShoesRepository::class)
 */
class Shoes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $category;

    /**
     * @ORM\Column(type="boolean",  nullable=true)
     */
    public $popular_arrivals_check = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    public $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SonataMediaMedia", cascade={"persist"}, fetch="LAZY")
     */
    public $media;

    /**
     * @param MediaInterface $media
     */
    public function setMedia(MediaInterface $media)
    {
        $this->media = $media;
    }

    /**
     * @return MediaInterface
     */
    public function getMedia()
    {
        return $this->media;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPopularArrivalsCheck()
    {
        return $this->popular_arrivals_check;
    }

    /**
     * @param mixed $popular_arrivals_check
     */
    public function setPopularArrivalsCheck($popular_arrivals_check): void
    {
        $this->popular_arrivals_check = $popular_arrivals_check;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }
}
