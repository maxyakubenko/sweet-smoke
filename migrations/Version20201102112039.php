<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201102112039 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE shoes ADD media_id INT DEFAULT NULL, DROP image');
        $this->addSql('ALTER TABLE shoes ADD CONSTRAINT FK_14CF8197EA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id)');
        $this->addSql('CREATE INDEX IDX_14CF8197EA9FDD75 ON shoes (media_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE shoes DROP FOREIGN KEY FK_14CF8197EA9FDD75');
        $this->addSql('DROP INDEX IDX_14CF8197EA9FDD75 ON shoes');
        $this->addSql('ALTER TABLE shoes ADD image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, DROP media_id');
    }
}
